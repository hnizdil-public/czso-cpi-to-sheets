import os

import requests
from google.oauth2 import service_account
from googleapiclient.discovery import build


class FlowError(Exception):
    pass


SPREADSHEET_ID: str = os.getenv('SPREADSHEET_ID')
SERVICE_ACCOUNT_FILE: str = os.getenv('SERVICE_ACCOUNT_FILE')


def main():
    creds = service_account.Credentials.from_service_account_file(SERVICE_ACCOUNT_FILE)

    try:
        spreadsheets = build('sheets', 'v4', credentials=creds).spreadsheets()
        starting_row, period_from, period_to = get_periods(spreadsheets)
        cpis = get_cpis(period_from, period_to)
        update_cpis(cpis, spreadsheets, starting_row)

    except FlowError as e:
        print(e)

    except Exception as e:
        print(e)


def get_periods(spreadsheets) -> (int, str, str):
    result = spreadsheets.values().get(spreadsheetId=SPREADSHEET_ID, range='Mzda!A3:C').execute()
    rows: [[str]] = result.get('values', [])
    for index, row in enumerate(rows):
        if len(row) < 3:
            period_from = format_period(row[0])
            period_to = format_period(rows[-1][0])
            return index + 3, period_from, period_to
    raise FlowError('No periods without CPI found')


def format_period(period: str) -> str:
    month, year = period.split(sep='/')
    return f'{year}{int(month):02d}'


def get_cpis(period_from: str, period_to: str) -> list[float]:
    r = requests.get('https://www.cnb.cz/cnb/STAT.ARADY_PKG.VYSTUP', params={
        'p_period': '1',
        'p_sort': '1',
        'p_des': '50',
        'p_sestuid': '21728',
        'p_uka': '1',
        'p_strid': 'ACBAA',
        'p_od': period_from,
        'p_do': period_to,
        'p_lang': 'CS',
        'p_format': '2',
        'p_decsep': '.',
    })
    rows_with_header: list[str] = r.text.strip().split('\n')
    return list(
        map(
            lambda row: float(row.split('|')[1]),
            rows_with_header[1:]
        )
    )


def update_cpis(cpis, spreadsheets, starting_row):
    rows = list(map(lambda cpi: [cpi], cpis))
    result = spreadsheets.values().update(
        spreadsheetId=SPREADSHEET_ID,
        range=f'Mzda!C{starting_row}:C',
        valueInputOption='USER_ENTERED',
        body={'values': rows},
    ).execute()
    print(result)


if __name__ == '__main__':
    main()
