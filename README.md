# Create virtualenv and install requirements

```shell
python3.11 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

# Activate virtualenv

```shell
source venv/bin/activate
```

# Install package and add to requirements

```shell
pip install google-api-python-client
pip freeze > requirements.txt
```
